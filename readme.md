#SMS Shopping List

This app is to allow my family, who seem to prefer to send text messages with shopping items, to text them to a place where they will be stored and displayed in a sensible format. Try going to the grocery store sometime with a shopping list split across 3 different text conversations. Not fun.

